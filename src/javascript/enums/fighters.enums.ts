export enum FightersClasses {
    Name = 'fighter___fighter-name',
    Root = 'fighters___root',
    List = 'fighters___list',
    Fighter = 'fighters___fighter',
    Image = 'fighter___fighter-image'
}

export enum FightersMessages {
    Fighter = 'Fighter',
    Won = 'won',
    GameOver = 'Game over'
}

export enum ElementsId {
    FighterIndicator = 'fighter-indicator'
}