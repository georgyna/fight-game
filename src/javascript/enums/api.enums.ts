export enum Api {
    LoadError = 'Failed to load',
    FightersEndpoint = 'fighters.json',
    DetailsEndpoint = 'details/fighter/',
    Json = '.json'
}

export enum ApiMethod {
    Get = 'GET'
}