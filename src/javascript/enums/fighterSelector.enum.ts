export enum FighterSelectorClasses {
    Root = 'preview-container___root',
    VersusBlock = 'preview-container___versus-block',
    VersusImg = 'preview-container___versus-img',
    FigthButton = 'preview-container___fight-btn'
}