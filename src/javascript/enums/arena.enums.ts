export enum ArenaClasses {
    Root = 'arena___root',
    FightStatus = 'arena___fight-status',
    VersusSign = 'arena___versus-sign',
    FighterIndicator = 'arena___fighter-indicator',
    FighterName = 'arena___fighter-name',
    HealthIndicator = 'arena___health-indicator',
    HealthBar = 'arena___health-bar',
    Battlefield = 'arena___battlefield',
    RightFighter = 'arena___right-fighter',
    LeftFighter = 'arena___left-fighter',
    Fighter = 'arena___fighter'
}

export enum ArenaSides {
    Left = 'left',
    Right = 'right'
}
