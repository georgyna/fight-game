export enum FighterPreviewClasses {
    Right = 'fighter-preview___right',
    Left = 'fighter-preview___left',
    Root = 'fighter-preview___root',
    Name = 'fighter___fighter-name',
    HealthDetails = 'fighter___fighter-details-health',
    AttackDetails = 'fighter___fighter-details-attack',
    DefenseDetails = 'fighter___fighter-details-defense',
    FighterDetails = 'fighter___fighter-details',
    Image = 'fighter-preview___img'
}

export enum PreviewDetailLabels {
    Health = 'Health',
    Attack = 'Attack',
    Defense = 'Defense'
}