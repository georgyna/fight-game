export enum DomElementsId {
  Root = 'root'
};

export enum ModalClasses {
  Layer = 'modal-layer',
  Root = 'modal-root',
  Header = 'modal-header',
  CloseBtn = 'close-btn',
  Body = 'modal-body'
}

export enum Tags {
  Div = 'div',
  Span = 'span',
  Img = 'img',
  Button = 'button'
}

export enum Events {
  Click = 'click',
  GameOver ='game-over',
  Keydown = 'keydown',
  Keyup = 'keyup'
}

export enum ButtonLabels {
  Close = '×',
  Fight = 'Fight'
}

export enum Attributes {
  Disabled = 'disabled'
}