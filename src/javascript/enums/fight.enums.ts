export enum FightMessages {
    CriticalHit = 'Critical hit available only once in 10 seconds',
    Warning = 'Warning'
}