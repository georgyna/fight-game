import { fightersDetails, fighters } from './mockData';
import { IFighter } from '../interfaces/fighter.interface';
import { Api } from '../enums/api.enums';
import { FighterResponse } from '../aliases/fighter.aliases';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;

async function callApi<T>(endpoint: string, method: string): Promise<T> {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error(Api.LoadError))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<FighterResponse> {
  const response = endpoint === Api.FightersEndpoint ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error(Api.LoadError))), 500);
  });
}

function getFighterById(endpoint: string): IFighter {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf(Api.Json);
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

export { callApi };
