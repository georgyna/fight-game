import { IDomElement } from "../interfaces/dom.interface";

export function createElement({ tagName, className, attributes = {} }: IDomElement): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (attributes) {
    Object.keys(attributes).forEach((key: string) => { element.setAttribute(key, attributes[key]) });
  }

  return element;
}
