export interface IAttribute {
  [key: string]: string;
}

export interface IHtmlAttributes extends IAttribute {
  id?: string;
  src?: string;
  title?: string;
  alt?: string;
}

export interface IDomElement {
  tagName: string;
  className?: string;
  attributes?: IHtmlAttributes;
}