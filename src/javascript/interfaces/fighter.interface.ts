import { ArenaSides } from "../enums/arena.enums";

export interface IPerson {
  _id: string;
  name: string;
  source: string;
}

export interface IFighter extends IPerson {
  attack: number;
  defense: number;
  health: number;
};

export interface IFighterParams {
  side: ArenaSides;
  initialHealth: number;
}

export interface IFightersParams {
  [name: string]: IFighterParams;
}
