import { IPerson, IFighter } from "../interfaces/fighter.interface";

export type FighterResponse = IPerson[] | IFighter;