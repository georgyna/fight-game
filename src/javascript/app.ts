import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { IPerson } from './interfaces/fighter.interface';
import { DomElementsId } from './enums/dom.enums';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById(DomElementsId.Root);
  static loadingElement = document.getElementById('loading-overlay');

  async startApp(): Promise<void> {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters: Array<IPerson> = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
