import { callApi } from '../helpers/apiHelper';
import { IFighter, IPerson } from '../interfaces/fighter.interface';
import { Api, ApiMethod } from '../enums/api.enums';

class FighterService {
  async getFighters(): Promise<Array<IPerson>> {
    try {
      const endpoint = Api.FightersEndpoint;
      const apiResult = await callApi<Array<IPerson>>(endpoint, ApiMethod.Get);

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighter> {
    try {
      const endpoint = `${Api.DetailsEndpoint}${id}${Api.Json}`;
      const apiResult = await callApi<IFighter>(endpoint, ApiMethod.Get);

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
