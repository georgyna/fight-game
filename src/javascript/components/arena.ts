import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighter } from '../interfaces/fighter.interface';
import { DomElementsId, Tags } from '../enums/dom.enums';
import { ArenaClasses, ArenaSides } from '../enums/arena.enums';
import { ElementsId } from '../enums/fighters.enums';

export function renderArena(selectedFighters: Array<IFighter>): void {
  const root = document.getElementById(DomElementsId.Root);
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(selectedFighters).then((winner: IFighter) => {
    showWinnerModal(winner);
  });
}

function createArena(selectedFighters: Array<IFighter>): HTMLElement {
  const arena = createElement({ tagName: Tags.Div, className: ArenaClasses.Root });
  const healthIndicators = createHealthIndicators(selectedFighters);
  const fighters = createFighters(selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators([leftFighter, rightFighter]: Array<IFighter>): HTMLElement {
  const healthIndicators = createElement({ tagName: Tags.Div, className: ArenaClasses.FightStatus });
  const versusSign = createElement({ tagName: Tags.Div, className: ArenaClasses.VersusSign });
  const leftFighterIndicator = createHealthIndicator(leftFighter, ArenaSides.Left);
  const rightFighterIndicator = createHealthIndicator(rightFighter, ArenaSides.Right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighter, position: string): HTMLElement {
  const { name } = fighter;
  const container = createElement({ tagName: Tags.Div, className: ArenaClasses.FighterIndicator });
  const fighterName = createElement({ tagName: Tags.Span, className: ArenaClasses.FighterName });
  const indicator = createElement({ tagName: Tags.Div, className: ArenaClasses.HealthIndicator });
  const bar = createElement({
    tagName: Tags.Div,
    className: ArenaClasses.HealthBar,
    attributes: { id: `${position}-${ElementsId.FighterIndicator}` }
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters([firstFighter, secondFighter]: Array<IFighter>): HTMLElement {
  const battleField = createElement({ tagName: Tags.Div, className: ArenaClasses.Battlefield });
  const firstFighterElement = createFighter(firstFighter, ArenaSides.Left);
  const secondFighterElement = createFighter(secondFighter, ArenaSides.Right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighter, position: string): HTMLElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === ArenaSides.Right ? ArenaClasses.RightFighter : ArenaClasses.LeftFighter;
  const fighterElement = createElement({
    tagName: Tags.Div,
    className: `${ArenaClasses.Fighter} ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
