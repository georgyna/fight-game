import { controls } from '../../constants/controls';
import { showModal } from './modal/modal';
import { createElement } from '../helpers/domHelper';
import { IFighter, IFightersParams, IFighterParams } from '../interfaces/fighter.interface';
import { Events, Tags, ModalClasses } from '../enums/dom.enums';
import { FightMessages } from '../enums/fight.enums';
import { ElementsId } from '../enums/fighters.enums';
import { ArenaSides } from '../enums/arena.enums';

export async function fight([firstFighter, secondFighter]: Array<IFighter>): Promise<IFighter> {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    fighting(firstFighter, secondFighter);
    document.addEventListener(Events.GameOver, (event: CustomEvent) => {
      resolve(event.detail);
    });
  });
}

export function getDamage(attacker: IFighter, defender: IFighter): number {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: IFighter): number {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: IFighter): number {
  // return block power
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

export function getCriticalDamage(attacker: IFighter, playersWithBlockedCritical: Map<string, boolean>): number {
  if (!playersWithBlockedCritical.has(attacker.name)) {
    playersWithBlockedCritical.set(attacker.name, true);
    setTimeout(() => {
      playersWithBlockedCritical.delete(attacker.name);
    }, 10000);
    return 2 * attacker.attack;
  } else {
    const wait10sec = createElement({ tagName: Tags.Span, className: ModalClasses.Body });
    wait10sec.innerText = FightMessages.CriticalHit;
    showModal({ title: FightMessages.Warning, bodyElement: wait10sec });
    return 0;
  }
}

export function changeFighterHealth(fighter: IFighter, damage: number, { side, initialHealth }: IFighterParams): void {
  const healthIndicator = document.getElementById(`${side}-${ElementsId.FighterIndicator}`);
  fighter.health = fighter.health - damage;
  const healthPercent = 100 - ((initialHealth - fighter.health) / fighter.health) * 100;
  if (healthPercent > 0) {
    healthIndicator.style.width = `${healthPercent}%`;
  } else {
    healthIndicator.style.width = '0';
    fighter.health = 0;
  }
}

export function fighting(firstFighter: IFighter, secondFighter: IFighter): void {
  let keyPressed = new Map();
  let playersWithBlockedCritical = new Map();
  const fightersParams: IFightersParams = {
    [firstFighter.name]: {
      side: ArenaSides.Left,
      initialHealth: firstFighter.health,
    },
    [secondFighter.name]: {
      side: ArenaSides.Right,
      initialHealth: secondFighter.health,
    },
  };
  document.addEventListener(
    Events.Keydown,
    (event) => {
      keyPressed.set(event.code, true);
      switch (event.code) {
        case controls.PlayerOneAttack: {
          if (!keyPressed.has(controls.PlayerOneBlock)) {
            const damage = keyPressed.has(controls.PlayerTwoBlock) ? 0 : getDamage(firstFighter, secondFighter);
            changeFighterHealth(secondFighter, damage, fightersParams[secondFighter.name]);
          }
          break;
        }
        case controls.PlayerTwoAttack: {
          if (!keyPressed.has(controls.PlayerTwoBlock)) {
            const damage = keyPressed.has(controls.PlayerOneBlock) ? 0 : getDamage(secondFighter, firstFighter);
            changeFighterHealth(firstFighter, damage, fightersParams[firstFighter.name]);
          }
          break;
        }
        default: {
          if (controls.PlayerOneCriticalHitCombination.every((control) => keyPressed.has(control))) {
            const damage = getCriticalDamage(firstFighter, playersWithBlockedCritical);
            changeFighterHealth(secondFighter, damage, fightersParams[secondFighter.name]);
          } else if (controls.PlayerTwoCriticalHitCombination.every((control) => keyPressed.has(control))) {
            const damage = getCriticalDamage(secondFighter, playersWithBlockedCritical);
            changeFighterHealth(firstFighter, damage, fightersParams[firstFighter.name]);
          }
          break;
        }
      }
      checkHealth(firstFighter, secondFighter);
    },
    true
  );
  document.addEventListener(Events.Keyup, (event) => {
    keyPressed.delete(event.code);
  });
}

function checkHealth(firstFighter: IFighter, secondFighter: IFighter): void {
  if (firstFighter.health <= 0) {
    document.dispatchEvent(new CustomEvent(Events.GameOver, { detail: secondFighter }));
  } else if (secondFighter.health <= 0) {
    document.dispatchEvent(new CustomEvent(Events.GameOver, { detail: firstFighter }));
  }
}
