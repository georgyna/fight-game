import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { IFighter } from '../../interfaces/fighter.interface';
import { Tags, ModalClasses } from '../../enums/dom.enums';
import { FightersClasses, FightersMessages } from '../../enums/fighters.enums';

export function showWinnerModal(fighter: IFighter): void {
  // call showModal function 
  const fighterName = createElement({ tagName: Tags.Span, className: FightersClasses.Name });
  fighterName.innerText = `${FightersMessages.Fighter} ${fighter.name} ${FightersMessages.Won}!`;
  const fighterImage = createFighterImage(fighter);
  const fighterDetails = createElement({ tagName: Tags.Div, className: ModalClasses.Body });
  fighterDetails.append(fighterName, fighterImage);
  showModal({ title: `${FightersMessages.GameOver}!`, bodyElement: fighterDetails });
}
