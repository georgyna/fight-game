import { createElement } from '../../helpers/domHelper';
import { DomElementsId, ModalClasses, Tags, Events, ButtonLabels } from '../../enums/dom.enums';

export interface IModalElement {
  title: string;
  bodyElement?: HTMLElement;
  onClose?: Function;
}

export function showModal({ title, bodyElement, onClose = () => {} }: IModalElement): void {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose }); 
  
  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById(DomElementsId.Root);
}

function createModal({ title, bodyElement, onClose }: IModalElement): HTMLElement {
  const layer = createElement({ tagName: Tags.Div, className: ModalClasses.Layer });
  const modalContainer = createElement({ tagName: Tags.Div, className: ModalClasses.Root });
  const header = createHeader({ title, onClose });

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader({ title, onClose }: IModalElement): HTMLElement {
  const headerElement = createElement({ tagName: Tags.Div, className: ModalClasses.Header });
  const titleElement = createElement({ tagName: Tags.Span });
  const closeButton = createElement({ tagName: Tags.Div, className: ModalClasses.CloseBtn });
  
  titleElement.innerText = title;
  closeButton.innerText = ButtonLabels.Close;
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener(Events.Click, close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal = document.getElementsByClassName(ModalClasses.Layer)[0];
  modal?.remove();
}
