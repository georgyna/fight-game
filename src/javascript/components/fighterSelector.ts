import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighter } from '../interfaces/fighter.interface';
import { FighterSelectorClasses } from '../enums/fighterSelector.enum';
import { ArenaSides } from '../enums/arena.enums';
import { Tags, Events, ButtonLabels, Attributes } from '../enums/dom.enums';
import { ImagesSources } from '../enums/sources.enums';

export function createFightersSelector(): Function {
  let selectedFighters: IFighter[] = [];

  return async (_event: Event, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Map<string, IFighter> = new Map();

export async function getFighterInfo(fighterId: string): Promise<IFighter> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (!fighterDetailsMap.has(fighterId)) {
    try {
      const fighterDetails = await fighterService.getFighterDetails(fighterId);
      fighterDetailsMap.set(fighterId, fighterDetails);
    } catch (error) {
      console.warn(error);
    }
    return fighterDetailsMap.get(fighterId);
  }
}

function renderSelectedFighters(selectedFighters: Array<IFighter>): void {
  const fightersPreview = document.querySelector(`.${FighterSelectorClasses.Root}`);
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, ArenaSides.Left);
  const secondPreview = createFighterPreview(playerTwo, ArenaSides.Right);
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Array<IFighter>): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: Tags.Div, className: FighterSelectorClasses.VersusBlock });
  const image = createElement({
    tagName: Tags.Img,
    className: FighterSelectorClasses.VersusImg,
    attributes: { src: ImagesSources.Versus },
  });
  const disabledBtn = canStartFight ? '' : Attributes.Disabled;
  const fightBtn = createElement({
    tagName: Tags.Button,
    className: `${FighterSelectorClasses.FigthButton} ${disabledBtn}`,
  });

  fightBtn.addEventListener(Events.Click, onClick, false);
  fightBtn.innerText = ButtonLabels.Fight;
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Array<IFighter>): void {
  renderArena(selectedFighters);
}
