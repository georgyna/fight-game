import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { IPerson } from '../interfaces/fighter.interface';
import { Tags, Events } from '../enums/dom.enums';
import { FightersClasses } from '../enums/fighters.enums';
import { FighterSelectorClasses } from '../enums/fighterSelector.enum';
import { IHtmlAttributes } from '../interfaces/dom.interface';

export function createFighters(fighters: Array<IPerson>): HTMLElement {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: Tags.Div, className: FightersClasses.Root });
  const preview = createElement({ tagName: Tags.Div, className: FighterSelectorClasses.Root });
  const fightersList = createElement({ tagName: Tags.Div, className: FightersClasses.List });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: IPerson, selectFighter: Function): HTMLElement {
  const fighterElement = createElement({ tagName: Tags.Div, className: FightersClasses.Fighter });
  const imageElement = createImage(fighter);
  const onClick = (event: MouseEvent) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener(Events.Click, onClick, false);

  return fighterElement;
}

function createImage(fighter: IPerson): HTMLElement {
  const { source, name } = fighter;
  const attributes: IHtmlAttributes = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: Tags.Img,
    className: FightersClasses.Image,
    attributes
  });

  return imgElement;
}