import { createElement } from '../helpers/domHelper';
import { IFighter } from '../interfaces/fighter.interface';
import { FighterPreviewClasses, PreviewDetailLabels } from '../enums/fighterPreview.enums';
import { ArenaSides } from '../enums/arena.enums';
import { Tags } from '../enums/dom.enums';

export function createFighterPreview(fighter: IFighter, position: string): HTMLElement {
  const positionClassName = position === ArenaSides.Right ? FighterPreviewClasses.Right : FighterPreviewClasses.Left;
  const fighterElement = createElement({
    tagName: Tags.Div,
    className: `${FighterPreviewClasses.Root} ${positionClassName}`,
  });

  if (fighter) {
    const imageElement = createFighterImage(fighter);
    const fighterName = createElement({ tagName: Tags.Span, className: FighterPreviewClasses.Name });
    fighterName.innerText = fighter.name;
    const indicator = createElement({ tagName: Tags.Div, className: FighterPreviewClasses.HealthDetails });
    indicator.innerText = `${PreviewDetailLabels.Health}: ${fighter.health}`;
    const attack = createElement({ tagName: Tags.Div, className: FighterPreviewClasses.AttackDetails });
    attack.innerText = `${PreviewDetailLabels.Attack}: ${fighter.attack}`;
    const defense = createElement({ tagName: Tags.Div, className: FighterPreviewClasses.DefenseDetails });
    defense.innerText = `${PreviewDetailLabels.Defense}: ${fighter.defense}`;
    const fighterDetails = createElement({ tagName: Tags.Div, className: FighterPreviewClasses.FighterDetails});
    fighterDetails.append(indicator, attack, defense);

    fighterElement.append(fighterName, imageElement, fighterDetails);
  }

  return fighterElement;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: Tags.Img,
    className: FighterPreviewClasses.Image,
    attributes
  });

  return imgElement;
}
